Loblaw Usecase, User-Registration


Description Of Use-Case Implementation –
1.	Used Spring JPA- “@Entity” objects to define the table structure and JPARepository/CrudRepository  utilities for all DB operations , defined some custom methods in our JPARepository/CrudRepository object but no single direct SQL queries in code.  

2.	Used standard Spring-Cloud-Connector Beans and utilities in @Configuration class to connect to DB instances in PCF (Example – MYSQL DB)  , instead of using custom static connection parameters wherever we connect to any cloud marketplace services .
https://cloud.spring.io/spring-cloud-connectors/spring-cloud-spring-service-connector.html

3.	Used standard Payload validation using “javax.validation” with @Valid annotations (along with @NotNull, @Lenghth etc to auto-validate all the incoming JSON Payload at the beginning , and generate custom validation messages dynamically .

4.	Built exception handing framework , using spring’s standard annotations - @ControllerAdvice and @ExceptionHandler etc , So that all major predefined Exceptions can have single point of exit from application perspective. 

5.	The rest URI defined for create and retrieve operations are as below

Create -
HTTP POST – https://{host+domain name}/users/registration 

Authorization- Spring-Security-Basics has been used  
(Username- admin , password - hRAMSg3RtEBB5f99A )

Content-Type – application/json


Retrieve -
HTTP GET - https://{host+domain name}/users/retrieval 

Authorization- Spring-Security-Basics has been used  
(Username- admin , password - hRAMSg3RtEBB5f99A )

Parameters Passed- Header

6.	Junit – Unit test cases, are using Mockito , to mock the dependent objects w.r.t any certain method. Unit test case writing is not fully completed, Its in progress .

7.	 Using Spring -Profiling for any variables that is static , but changes env to env.
Ex- Logging level is set to INFO at UAT but ERROR at prod properties 

 

 


