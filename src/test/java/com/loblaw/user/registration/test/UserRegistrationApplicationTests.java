package com.loblaw.user.registration.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

import com.loblaw.user.registration.controller.UserRegistrationController;
import com.loblaw.user.registration.entity.domains.UserDetails;
import com.loblaw.user.registration.handlers.CustomValidationHandler;
import com.loblaw.user.registration.service.navigator.UserRegistrationServiceNavigator;
import com.loblaw.user.registration.sync.req_resp.domains.Data;
import com.loblaw.user.registration.sync.req_resp.domains.UserRegistrationInput;
import com.loblaw.user.registration.util.CommonUtils;
import com.loblaw.user.registration.util.UserRegistrationUtil;



@RunWith(SpringRunner.class)
@TestPropertySource(locations="classpath:application-local.properties")
public class UserRegistrationApplicationTests {

	@InjectMocks
	private UserRegistrationController userRegistrationController;



	@Mock
	UserRegistrationServiceNavigator userRegistrationServiceNavigator;

	@Mock
	CommonUtils commonUtils;

	@Mock
	UserRegistrationUtil userRegistrationUtil;

	@Mock
	CustomValidationHandler validationHandler;

	@Mock
	Errors errors;

	@Mock
	UserDetails userDetails;

	private UserRegistrationInput userRegistrationInput = new UserRegistrationInput();
	private HttpHeaders httpHeaders = new HttpHeaders();

	@Before
	public void executedBeforeEach() {


		userRegistrationInput.setCorrelationId("test_999_user_registration");
		userRegistrationInput.setFirstName("Arijit");
		userRegistrationInput.setLastName("Maity");
		userRegistrationInput.setAddress("Trinath Nuilding, Kolkata");
		userRegistrationInput.setTelephone("9874128821");
		userRegistrationInput.setEmail("arijit.maity@abcd.com");

		httpHeaders.add("correlationId", "test_999_user_retrieval");
		httpHeaders.add("email", "arijit.maity@abcd.com");
		
		
		Mockito.when(userDetails.getEmail()).thenReturn("arijit.maity@abcd.com");
		Mockito.when(userDetails.getAddress()).thenReturn("Trinath Nuilding, Kolkata");
		Mockito.when(userDetails.getCorrelationId()).thenReturn("test_999_user_retrieval");
		Mockito.when(userDetails.getFirstName()).thenReturn("Arijit");
		Mockito.when(userDetails.getLastName()).thenReturn("Maity");
	}

	//Test Cases For Create Flow
	
	//Test Create User Registration Response is Not-Null
	@Test
	public void testUserRegistrationResponse_NotNull() {
		Mockito.when(validationHandler.userRegistrationCustomValidator(userRegistrationInput, errors)).thenReturn(errors);
		assertNotNull(userRegistrationController.performUserRegistration(userRegistrationInput, errors));
	}

	//Test Create User Registration Response is Successful - HTTP -201
	@Test
	public void testUserRegistrationResponseStatus_Success() {
		Mockito.when(validationHandler.userRegistrationCustomValidator(userRegistrationInput, errors)).thenReturn(errors);
		Mockito.when(errors.hasErrors()).thenReturn(false);
		assertEquals(HttpStatus.CREATED, userRegistrationController.performUserRegistration(userRegistrationInput, errors).getStatusCode());
	}


	//Test Create User Registration Response Body is  - Not Null
	@Test
	public void testUserRegistrationResponseBody_NotNull() {
		Mockito.when(validationHandler.userRegistrationCustomValidator(userRegistrationInput, errors)).thenReturn(errors);
		Mockito.when(errors.hasErrors()).thenReturn(false);
		assertNotEquals(null, userRegistrationController.performUserRegistration(userRegistrationInput, errors).getBody());
	}

	
	//Test Cases For Retrieve Flow
	
	//Test User Retrieval Response is Not-Null
	@Test
	public void testUserRetrievalResponse_NotNull() {
		Mockito.when(userRegistrationServiceNavigator.performSingleUserRetrival(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(userDetails);
		assertNotNull(userRegistrationController.performUserRerival(httpHeaders));
	}
	
	//Test Retrieve User Response is Successful - HTTP -200
	@Test
	public void testUserRetrievalResponseStatus_Success() {
		Mockito.when(userRegistrationServiceNavigator.performSingleUserRetrival(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(userDetails);
		assertEquals(HttpStatus.OK ,userRegistrationController.performUserRerival(httpHeaders).getStatusCode());
	}
	
	//Test Retrieve User Registration Response Body is  - Not Null
	@Test
	public void testUserRetrievalResponseBody_NotNull() {
		Mockito.when(userRegistrationServiceNavigator.performSingleUserRetrival(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(userDetails);
		assertNotEquals(null, userRegistrationController.performUserRerival(httpHeaders).getBody());
	}
}