package com.loblaw.user.registration.handlers;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import com.loblaw.user.registration.sync.req_resp.domains.UserRegistrationInput;

/*
 * CustomValidationHandler is an common Input Validation handling class for any custom validation rules
*/


@Component
public class CustomValidationHandler {


	public Errors userRegistrationCustomValidator(UserRegistrationInput obj, Errors errors) {

		if (obj.getEmail() != null  && !obj.getEmail().isEmpty()) {

			if (!obj.getEmail().contains("@")) {
				errors.rejectValue("email", "Ivalid EmailId", ValidationMessageConstants.conditionalValidation);
			}

		}
		return errors;

	}
	
	
	
	public static Errors validateAndGenerateErrors(HttpHeaders httpHeaders , Errors errors , String correlationId) {


		String email = null;

		if (null != httpHeaders.get("email")) {
			email = httpHeaders.get("email").get(0);
		}

		if (StringUtils.isEmpty(email) ) {
			
			
			errors.rejectValue("email", "Required field is empty", ValidationMessageConstants.mandatory);

		}
		
		return errors;
	}

}