package com.loblaw.user.registration.service.navigator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.loblaw.user.registration.config.Config;
import com.loblaw.user.registration.entity.domains.UserDetails;
import com.loblaw.user.registration.entity.service.UserRegistrationRepositoryService;
import com.loblaw.user.registration.sync.req_resp.domains.UserRegistrationInput;
import com.loblaw.user.registration.util.CommonUtils;
import com.loblaw.user.registration.util.Constants;

//UserRegistrationServiceNavigator class handling the service-layer and annotated with @Service


@Service
public class UserRegistrationServiceNavigator {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	UserRegistrationRepositoryService assetRepositoryService;

	@Autowired
	Constants constants;

	@Autowired
	CommonUtils commonUtils;

	@Value("${logging.audit.request.response.trace}")
	boolean trace;

	@Autowired
	Config config;

	public void performUserRegistration(UserRegistrationInput input, UserDetails userDetails) {


		String correlationID = input.getCorrelationId();


		boolean uploadInitialDetailsSuccess = uploadInitialDetailsToUserDB ( input, userDetails, correlationID);

		if(uploadInitialDetailsSuccess) {

			logger.info("succesful DB upload"
					+ correlationID+ " and emailId- "+ input.getEmail());

		}
		else {
			logger.error("error occurred during initial details upload to TRAMAS DB for correlation Id: "
					+ correlationID+ " and emailId- "+ input.getEmail());
		}
	}


	public UserDetails performSingleUserRetrival(String email , String correlationId,
			UserDetails userDetails) {


		userDetails = getAssetDetailsForSpecificEmail(email);

		return userDetails;
	}

	private boolean uploadInitialDetailsToUserDB(UserRegistrationInput input, UserDetails userDetails, String correlationID) {

		try {
			if (null != userDetails && null != input ) {
				//Map input object to Entity Object 
				userDetails = config.mapUserRegistrationInputToEntity(input);

			}

			logger.info("uploading user details in DB during registration process for correlation Id: " + correlationID);
			uploadUserDetails(userDetails, correlationID);

		} catch (Exception e) {
			logger.error("error occurred during initial details upload to TRAMAS DB for correlation Id: "
					+ correlationID + "exception is " + e);
			return false;
		}
		return true;
	}




	private void uploadUserDetails(UserDetails userDetails , String correlationID) {
		try {
			assetRepositoryService.uploadSingleUserDetails(userDetails, correlationID);
		} catch (Exception e) {
			logger.error("Upload User details failed for correlation id" + correlationID
					+ " emailId - " + userDetails.getEmail() + "with exception" + e);
		}
	}

	private List<UserDetails> getAssetDetailsForSpecificFirstName(String firstName) {
		List<UserDetails> assetDetailsList = assetRepositoryService.getUserDetailsForSpecificFirstName(firstName);
		return assetDetailsList;
	}



	private UserDetails getAssetDetailsForSpecificEmail(String email) {
		UserDetails userDetails = assetRepositoryService.getUserDetailsForSpecificEmail(email);
		return userDetails;
	}

}
