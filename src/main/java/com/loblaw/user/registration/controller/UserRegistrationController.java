package com.loblaw.user.registration.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.loblaw.user.registration.entity.domains.UserDetails;
import com.loblaw.user.registration.handlers.CustomValidationHandler;
import com.loblaw.user.registration.service.navigator.UserRegistrationServiceNavigator;
import com.loblaw.user.registration.sync.req_resp.domains.Data;
import com.loblaw.user.registration.sync.req_resp.domains.UserRegistrationResponseOutput;
import com.loblaw.user.registration.sync.req_resp.domains.RetrivalResponseOutput;
import com.loblaw.user.registration.sync.req_resp.domains.UserRegistrationInput;
import com.loblaw.user.registration.util.CommonUtils;
import com.loblaw.user.registration.util.UserRegistrationUtil;




@RestController
@RefreshScope
public class UserRegistrationController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@InitBinder
	private void activateDirectFieldAccess(DataBinder dataBinder) {
		dataBinder.initDirectFieldAccess();
	}

	@Value("${logging.audit.request.response.trace}")
	boolean trace;

	@Autowired
	UserRegistrationServiceNavigator userRegistrationServiceNavigator;

	@Autowired
	UserRegistrationUtil assetLockUtil;

	@Autowired
	CommonUtils commonUtils;

	@Autowired
	CustomValidationHandler validationHandler;

	@PostMapping(path = "users/registration", consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<UserRegistrationResponseOutput> performUserRegistration(@Valid @RequestBody UserRegistrationInput input,
			Errors errors) {

		UserRegistrationResponseOutput response = new UserRegistrationResponseOutput();
		String correlationID = input.getCorrelationId();

		if (errors.hasErrors()) {
			logger.error("Received user registration Request with " + errors.getErrorCount() + " error(s), CorrelationID - "
					+ correlationID);
			return commonUtils.throwError(errors, HttpStatus.BAD_REQUEST, correlationID);
		} else {
			Errors customErrors = validationHandler.userRegistrationCustomValidator(input, errors);
			if (customErrors.hasErrors()) {
				logger.error("Received user registration Request with " + customErrors.getErrorCount()
				+ " custom error(s), CorrelationID - " + correlationID);
				return commonUtils.throwConditionalError(customErrors, HttpStatus.BAD_REQUEST, correlationID);
			}
		}

		response.setCorrelationId(correlationID);

		if (trace) {
			logger.info("Going for actual user registration for , CorrelationID - " + correlationID
					+ ", Request JSON String - " + UserRegistrationUtil.getJsonString(input));
		} else {
			logger.info("Going for Perform user registration for , CorrelationID - " + correlationID);
		}


		UserDetails userDetails = new UserDetails();

		userRegistrationServiceNavigator.performUserRegistration (input, userDetails) ;

		HttpStatus status = HttpStatus.CREATED;

		response.setStatus("Success");
		response.setTimestamp(commonUtils.getCurrentDateTime());

		return new ResponseEntity<UserRegistrationResponseOutput>(response, status);

	}



	@RequestMapping(path = "users/retrieval", method = RequestMethod.GET)
	public ResponseEntity<RetrivalResponseOutput> performUserRerival(@RequestHeader HttpHeaders httpHeaders) {

		String correlationId = null;
		String email = null ;

		try{

			RetrivalResponseOutput response = new RetrivalResponseOutput();

			correlationId = httpHeaders.get("correlationId").get(0);

			if(correlationId != null && !correlationId.isEmpty()) {
				response.setCorrelationId(correlationId);
			}

			/*Errors customErrors = validationHandler.validateAndGenerateErrors(httpHeaders , errors, correlationId);

			
				if (customErrors.hasErrors()) {
					logger.error("Received user retrieval Request with " + customErrors.getErrorCount()
					+ " custom error(s) - ");
					return commonUtils.throwConditionalErrorForRetrival(customErrors, HttpStatus.BAD_REQUEST, correlationId);
				}
			 */
			email = httpHeaders.get("email").get(0);

			UserDetails userDetails = new UserDetails();

			userDetails = userRegistrationServiceNavigator.performSingleUserRetrival(email , correlationId , userDetails) ;

			if (userDetails != null && userDetails.getEmail() !=null && !userDetails.getEmail().isEmpty()) {

				Data data = new Data ();
				data.setEmail(userDetails.getEmail());
				data.setAddress(userDetails.getAddress());
				data.setFirstName(userDetails.getFirstName());
				data.setLastName(userDetails.getLastName());
				data.setTelephone(userDetails.getTelephone());

				if(data !=null && data.getEmail()!=null && !data.getEmail().isEmpty()) {
					response.setData(data);
				}

				HttpStatus status = HttpStatus.OK;

				response.setStatus("Success");
				response.setTimestamp(commonUtils.getCurrentDateTime());

				return new ResponseEntity<RetrivalResponseOutput>(response, status);
			}

			else {
				response.setStatus("Failure");
				response.setTimestamp(commonUtils.getCurrentDateTime());

				return commonUtils.dataNotFoundError(correlationId, email, response);
			}

		}

		catch (Exception e) {

			logger.info("Exception occured in Retrival flow for correlationId::::: " + correlationId
					+ " emailId - " + e);
			throw e;

		}


	}

}
