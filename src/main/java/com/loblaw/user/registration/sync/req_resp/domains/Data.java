
package com.loblaw.user.registration.sync.req_resp.domains;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.loblaw.user.registration.handlers.ValidationMessageConstants;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Length;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"firstName",
	"lastName",
	"address",
	"telephone",
	"email"
})
public class Data {



	/**
	 * 
	 * (Required)
	 * 
	 */

	@JsonProperty("firstName")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String firstName;


	@JsonProperty("lastName")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String lastName;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("address")
	@Length(min = 1, max = 100)
	private String address;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("telephone")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 15)
	private String telephone;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("email")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String email;


	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();




	/**
	 * 
	 * (Required)
	 * 
	 */

	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("lastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("telephone")
	public String getTelephone() {
		return telephone;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("telephone")
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * (Required)
	 * 
	 */


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(lastName).append(address).append(telephone).append(email).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Data) == false) {
			return false;
		}
		Data rhs = ((Data) other);
		return new EqualsBuilder().append(lastName, rhs.lastName).append(address, rhs.address).append(telephone, rhs.telephone).append(email, rhs.email).append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}

