package com.loblaw.user.registration.sync.req_resp.domains;
/*//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.27 at 05:20:43 PM IST 
//


package com.telstra.cci.create.domains;

import javax.xml.bind.annotation.XmlRegistry;


*//**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.telstra.tdcrr.create.domains package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 *//*
@XmlRegistry
public class ObjectFactory {


    *//**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.telstra.tdcrr.create.domains
     * 
     *//*
    public ObjectFactory() {
    }

    *//**
     * Create an instance of {@link CreateRepairResponse }
     * 
     *//*
    public CreateRepairResponse createCreateRepairResponse() {
        return new CreateRepairResponse();
    }

    *//**
     * Create an instance of {@link SiebelMessageOut }
     * 
     *//*
    public SiebelMessageOut createSiebelMessageOut() {
        return new SiebelMessageOut();
    }

    *//**
     * Create an instance of {@link CreateRepairRequest }
     * 
     *//*
    public CreateRepairRequest createCreateRepairRequest() {
        return new CreateRepairRequest();
    }

    *//**
     * Create an instance of {@link SiebelMessage }
     * 
     *//*
    public SiebelMessage createSiebelMessage() {
        return new SiebelMessage();
    }

    *//**
     * Create an instance of {@link CCBCreateRRResponse }
     * 
     *//*
    public CCBCreateRRResponse createCCBCreateRRResponse() {
        return new CCBCreateRRResponse();
    }

    *//**
     * Create an instance of {@link CCBRepairRequestOutput }
     * 
     *//*
    public CCBRepairRequestOutput createCCBRepairRequestOutput() {
        return new CCBRepairRequestOutput();
    }

    *//**
     * Create an instance of {@link CCBCreateRRRequest }
     * 
     *//*
    public CCBCreateRRRequest createCCBCreateRRRequest() {
        return new CCBCreateRRRequest();
    }

    *//**
     * Create an instance of {@link CCBRepairRequestInput }
     * 
     *//*
    public CCBRepairRequestInput createCCBRepairRequestInput() {
        return new CCBRepairRequestInput();
    }

    *//**
     * Create an instance of {@link CCBRepairRequestSymptomInput }
     * 
     *//*
    public CCBRepairRequestSymptomInput createCCBRepairRequestSymptomInput() {
        return new CCBRepairRequestSymptomInput();
    }

    *//**
     * Create an instance of {@link CCBRepairRequestNoteInput }
     * 
     *//*
    public CCBRepairRequestNoteInput createCCBRepairRequestNoteInput() {
        return new CCBRepairRequestNoteInput();
    }

}
*/