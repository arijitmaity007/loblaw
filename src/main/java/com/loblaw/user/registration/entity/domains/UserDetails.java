package com.loblaw.user.registration.entity.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

//UserDetails is the @Entity class and it defines the table structure in DB, Table gets auto-created with the below definition.

@Entity
@Table(indexes = {@Index(name = "firstName",  columnList="firstName", unique = false),
        @Index(name = "lastName", columnList="lastName", unique = false),
		@Index(name = "correlationId", columnList="correlationId", unique = true)})
// Version 1 Of Entity
public class UserDetails {
	
	@Id
	protected String email ="";
    protected String firstName = "";
    
    protected String lastName = "";
    protected String address = "";
    protected String telephone = "";
    protected String correlationId = "";
    
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
    

}
