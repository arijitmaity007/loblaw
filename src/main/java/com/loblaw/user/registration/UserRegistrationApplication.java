package com.loblaw.user.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.loblaw.user.registration.entity.domains.UserDetailsRepository;


@SpringBootApplication(scanBasePackages="com.loblaw")
@EntityScan(basePackageClasses=com.loblaw.user.registration.entity.domains.UserDetails.class)
@EnableJpaRepositories( basePackageClasses = UserDetailsRepository.class )
@RefreshScope
@EnableHystrix
@EnableCircuitBreaker
public class UserRegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserRegistrationApplication.class, args);
	}
}
