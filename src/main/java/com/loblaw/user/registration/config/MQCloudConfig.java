package com.loblaw.user.registration.config;
/*package com.loblaw.asset.tramas.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.converter.MessageConverter;

@Configuration
@Profile("cloud")
public class MQCloudConfig extends AbstractCloudConfig{

    
    @Autowired
    EnvConfig vcapConfig;

    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
    @Bean
	public ActiveMQConnectionFactory connectionFactory(){
	    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    
	    System.out.println("vcapConfig.BROKER_URL" + vcapConfig.BROKER_URL);
	    System.out.println("User" + vcapConfig.BROKER_USERNAME);
	    System.out.println("Pass" + vcapConfig.BROKER_PASSWORD);
	    
	    connectionFactory.setBrokerURL(vcapConfig.BROKER_URL);
	    connectionFactory.setUserName(vcapConfig.BROKER_USERNAME);
	    connectionFactory.setPassword(vcapConfig.BROKER_PASSWORD);
	    return connectionFactory;
	}
    
  
    @Bean
    public ConnectionFactory rabbitFactory() {
        return connectionFactory().rabbitConnectionFactory();
    }
    
    @Bean
    public RabbitTemplate rabbitTemplate() {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(rabbitFactory());
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }
    
}*/