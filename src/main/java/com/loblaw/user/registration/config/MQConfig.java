package com.loblaw.user.registration.config;
/*package com.loblaw.asset.tramas.config;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;

import com.loblaw.asset.tramas.util.Constants;

@Configuration
@Profile("local")
public class MQConfig {


    @Bean
    Queue queue() {
        return new Queue("trial", false);
    }

    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange(Constants.OUTBOUND_QUEUE_TOPIC_EXCHANGE);
    }

    @Bean
    DirectExchange directExchange() {
    	return new DirectExchange(Constants.OUTBOUND_QUEUE_DIRECT_EXCHANGE);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(Constants.OUTBOUND_QUEUE_ROUTING_KEY);
    }

    @Bean
    public ConnectionFactory connectionFactory() throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setAddresses("");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        return connectionFactory;
        
        //connectionFactory.getRabbitConnectionFactory().setHost("localhost");
        //connectionFactory.getRabbitConnectionFactory().setPort(5672);
        connectionFactory.getRabbitConnectionFactory().setUsername("guest");
        connectionFactory.getRabbitConnectionFactory().setPassword("guest");
       //connectionFactory.getRabbitConnectionFactory().setUri("amqps://guest:guest@localhost:5672/virtualhost");
		return connectionFactory;
    }
    
    
    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }


}*/