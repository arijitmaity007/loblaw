package com.loblaw.user.registration.util;

import org.json.JSONObject;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserRegistrationUtil {
	

	public static String getJsonString(Object obj) 
	{
		JSONObject jsonObj = new JSONObject(obj);
		return jsonObj.toString();
	}
	
	
}
