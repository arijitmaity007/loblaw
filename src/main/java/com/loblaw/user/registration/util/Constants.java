package com.loblaw.user.registration.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * Constants class.
 */
@RefreshScope
@Configuration
public class Constants {
	
	public static final String OUTBOUND_QUEUE_DIRECT_EXCHANGE = "loblawUsecaseDirectExchange";
	public static final String OUTBOUND_QUEUE_TOPIC_EXCHANGE = "loblawUsecaseTopicExchange";

	public static final String OUTBOUND_QUEUE_ROUTING_KEY = "loblawUsecaseTargetSystemRoutingKey";


}
