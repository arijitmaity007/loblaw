package com.loblaw.user.registration.util;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.loblaw.user.registration.sync.req_resp.domains.UserRegistrationResponseOutput;
import com.loblaw.user.registration.sync.req_resp.domains.RetrivalResponseOutput;

/*
 * CommonUtils is a class which has common Utility functions  
*/

@Service
public class CommonUtils {

	public String getCurrentDateTime(){

		DateTime currentDate = new DateTime().withZone (DateTimeZone.forID("Australia/Melbourne"));
		String format = "yyyy-MM-dd'T'HH:mm:ssZZ";
		return currentDate.toString(format);
	}
	
	public String getCurrentGMTDateTime(){

		DateTime currentDate = new DateTime().withZone (DateTimeZone.forID("Etc/GMT"));
		String format = "yyyy-MM-dd'T'HH:mm:ssZZ";
		return currentDate.toString(format);
	}
	
	public String getJsonString (Object obj) 
	{
		JSONObject jsonObj = new JSONObject(obj);
		return jsonObj.toString();
	}
	
	public ResponseEntity<UserRegistrationResponseOutput> throwConditionalError (Errors errors, HttpStatus httpStatus, String correlationID) 
	{
		UserRegistrationResponseOutput errorMessage = new UserRegistrationResponseOutput();
		errorMessage.setCorrelationId(correlationID);
		errorMessage.setStatus(httpStatus.toString());
		errorMessage.setResponseCode("BADREQUEST");
		errorMessage.setTimestamp(getCurrentDateTime());

		List <com.loblaw.user.registration.sync.req_resp.domains.Errors> errorList = new ArrayList<com.loblaw.user.registration.sync.req_resp.domains.Errors>();
		for (FieldError error : errors.getFieldErrors()) {
			com.loblaw.user.registration.sync.req_resp.domains.Errors msg = new com.loblaw.user.registration.sync.req_resp.domains.Errors();
			String[] field = error.getField().split("\\.");
			msg.setCode("FV002");
			msg.setMessage(field[field.length - 1] + " - " + error.getDefaultMessage());
			errorList.add(msg);
		}
		errorMessage.setErrors(errorList);
		return new ResponseEntity<>(errorMessage, httpStatus);
	}
	
	
	public ResponseEntity<RetrivalResponseOutput> throwConditionalErrorForRetrival (Errors errors, HttpStatus httpStatus, String correlationID) 
	{
		RetrivalResponseOutput errorMessage = new RetrivalResponseOutput();
		errorMessage.setCorrelationId(correlationID);
		errorMessage.setStatus(httpStatus.toString());
		errorMessage.setResponseCode("BADREQUEST");
		errorMessage.setTimestamp(getCurrentDateTime());

		List <com.loblaw.user.registration.sync.req_resp.domains.Errors> errorList = new ArrayList<com.loblaw.user.registration.sync.req_resp.domains.Errors>();
		for (FieldError error : errors.getFieldErrors()) {
			com.loblaw.user.registration.sync.req_resp.domains.Errors msg = new com.loblaw.user.registration.sync.req_resp.domains.Errors();
			String[] field = error.getField().split("\\.");
			msg.setCode("FV002");
			msg.setMessage(field[field.length - 1] + " - " + error.getDefaultMessage());
			errorList.add(msg);
		}
		errorMessage.setErrors(errorList);
		return new ResponseEntity<>(errorMessage, httpStatus);
	}

	public ResponseEntity<UserRegistrationResponseOutput> throwError(Errors errors, HttpStatus httpStatus, String correlationID) 
	{
		UserRegistrationResponseOutput errorMessage = new UserRegistrationResponseOutput();
		errorMessage.setCorrelationId(correlationID);
		errorMessage.setStatus(httpStatus.toString());
		errorMessage.setResponseCode("BADREQUEST");
		errorMessage.setTimestamp(getCurrentDateTime());

		List<com.loblaw.user.registration.sync.req_resp.domains.Errors> errorList = new ArrayList<com.loblaw.user.registration.sync.req_resp.domains.Errors>();
		for (FieldError error : errors.getFieldErrors()) {
			com.loblaw.user.registration.sync.req_resp.domains.Errors msg = new com.loblaw.user.registration.sync.req_resp.domains.Errors();
			String[] field = error.getField().split("\\.");
			msg.setCode("FV001");
			msg.setMessage(field[field.length - 1] + " - " + error.getDefaultMessage());
			errorList.add(msg);
		}
		errorMessage.setErrors(errorList);
		return new ResponseEntity<>(errorMessage, httpStatus);
	}
	
	
	public ResponseEntity<RetrivalResponseOutput> dataNotFoundError(String correlationId ,  String emailId , RetrivalResponseOutput retrivalResponseOutput) {
		
		com.loblaw.user.registration.sync.req_resp.domains.Errors errors = new com.loblaw.user.registration.sync.req_resp.domains.Errors();
		List<com.loblaw.user.registration.sync.req_resp.domains.Errors> errorList = new ArrayList<com.loblaw.user.registration.sync.req_resp.domains.Errors>();

		errors.setCode("404");
		errors.setMessage("Data Not Found with - "+emailId);
		
		errorList.add(errors);
		retrivalResponseOutput.setErrors(errorList);
		
		return new ResponseEntity<RetrivalResponseOutput>(retrivalResponseOutput, HttpStatus.NOT_FOUND);
	}
}
